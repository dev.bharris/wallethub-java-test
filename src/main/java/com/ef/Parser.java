package com.ef;
import java.sql.*;
import java.util.*;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Date;


class Parser
{


   public static Date parseDate(SimpleDateFormat sdf, String date) {
     Date newDate = null;
      try {
        newDate = sdf.parse(date);
     } catch (Exception e) {
         newDate = null;
        System.out.println(e);
     } finally {
     }
     return newDate;
   }

   public static Date getEndDate(Date startDate, String duration) {

      Date endDate = null;
      switch (duration) {
         case "hourly":
            endDate = new Date(startDate.getTime() + (3600*1000));
            break;
         case "daily":
            endDate = new Date(startDate.getTime() + 24 * (3600*1000));
            break;
         default:
            System.out.println("Error getting end date");

      }

      return endDate;
   }

   public static Connection getMySqlConnection() {
      Connection conn = null;
      try {
         conn =  DriverManager.getConnection(
                 "jdbc:mysql://localhost:3306/wallethub?useSSL=false&useUnicode=true" +
                         "&useJDBCCompliantTimezoneShift=true" +
                         "&useLegacyDatetimeCode=false" +
                         "&serverTimezone=UTC", "wallethub_usr1", "spendspendspend");
      } catch (Exception e) {
         System.err.println(e);
      }
      return conn;
   }

   public static void clearTable(Connection conn, String tableName) {
      System.out.println("Clearing Access_Log Table");

      try
      {
         String query = "delete from " + tableName;
         PreparedStatement preparedStmt = conn.prepareStatement(query);
         preparedStmt.execute();
      }
      catch (Exception e)
      {
         System.err.println(e.getMessage());
      }
   }

   public static void writeAccessLog(Connection conn, Date time, String ip, String usr_agent) {

      try {

         // the mysql insert statement
         String query = " insert into access_log (access_date, ip, usr_agent)"
                 + " values (?, ?, ?)";

         SimpleDateFormat logSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

         // create the mysql insert preparedstatement
         PreparedStatement preparedStmt = conn.prepareStatement(query);
         preparedStmt.setString   (1, logSdf.format(time).toString());
         preparedStmt.setString (2, ip);
         preparedStmt.setString (3, usr_agent);

         // execute the preparedstatement
         preparedStmt.execute();

      }
      catch (Exception e)
      {
         System.err.println(e.getMessage());
      }
   }

   public static void writeRiskLog(Connection conn, Date sdfStartDate, String threshold, String duration, String ip) {

      System.out.println("Failed to pass filter: " + ip);
      try {

         // the mysql insert statement
         String query = " insert into risk_log (ip, comments)"
                 + " values ( ?, ?)";

         // create the mysql insert preparedstatement
         PreparedStatement preparedStmt = conn.prepareStatement(query);
         preparedStmt.setString   (1, ip);
         preparedStmt.setString (2, "Added by java program. Filter values: start date: " +
                         sdfStartDate.toString() + ". duration:" + duration + ". threshold:" + threshold + ".");

         // execute the preparedstatement
         preparedStmt.execute();

      }
      catch (Exception e)
      {
         System.err.println(e.getMessage());
      }
   }

   public static List getRiskIps(Connection conn, Date startTime, Date endtime, String threshold) {

      List<String> riskIps = new ArrayList<String>();
      try
      {
         SimpleDateFormat logSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

         String query = "select ip, count(*) as count from access_log where access_date > '" +
                 logSdf.format(startTime).toString() + "' and access_date < '" +
                 logSdf.format(endtime).toString() + "' GROUP BY ip having count > " + threshold + ";";

         // create the java statement
         Statement st = conn.createStatement();

         // execute the query, and get a java resultset
         ResultSet rs = st.executeQuery(query);

         // iterate through the java resultset
         while (rs.next())
         {
            String ip = rs.getString("ip");
            riskIps.add(ip);
         }
         st.close();
      }
      catch (Exception e)
      {
         System.err.println(e.getMessage());
      }

      return riskIps;
   }

   public static void main(String args[])
   {
      System.out.println("Begining the parsing");
      SimpleDateFormat cmdSdf = new SimpleDateFormat("yyyy-MM-dd.HH:mm:ss");
      SimpleDateFormat logSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss");

      //Assuming all arguments are not required initialize their values to default values
      String logPath = "";
      String startDate = "2017-01-01.13:00:00";
      String duration = "hourly";
      String threshold = "50";


      for (String var : args) {

         switch (var.split("=")[0]) {
            case "--accesslog":
               logPath = var.split("=")[1];
               System.out.println("Setting logPath to: " + logPath);
               break;
            case "--startDate":
               startDate = var.split("=")[1];
               System.out.println("Setting startDate to: " + startDate);
               break;
            case "--duration":
               duration = var.split("=")[1];
               System.out.println("Setting duration to: " + duration);
               break;
            case "--threshold":
               threshold = var.split("=")[1];
               System.out.println("Setting threshold to: " + threshold);
               break;
            default:
               System.out.println("Unknown command line argument");
               break;
         }

      }

      Date sdfStartDate = parseDate(cmdSdf, startDate);
      Date sdfEndDate = getEndDate(sdfStartDate, duration);

      FileInputStream inputStream = null;
      Scanner sc = null;
      Connection conn = getMySqlConnection();
      try {
         inputStream = new FileInputStream(logPath + "/access.log");
         sc = new Scanner(inputStream, "UTF-8");
         clearTable(conn, "access_log");
         while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String tmp[] = line.split("\\|");
            if((tmp != null) && (tmp.length == 5)) {
               writeAccessLog(conn, logSdf.parse(tmp[0]), tmp[1], tmp[4]);
            } else {
               System.err.println("Unexpected log line format");
            }
         }

         List<String> riskIps = getRiskIps(conn, sdfStartDate, sdfEndDate,threshold);
         for (String ip : riskIps){
            writeRiskLog(conn, sdfStartDate, threshold, duration, ip);
         }
         // TODO: Scanner surpresses exceptions
         if (sc.ioException() != null) {
            throw sc.ioException();
         }
      } catch (Exception e) {
         System.out.println(e);
      } finally {

         if (inputStream != null) {
            try {
               inputStream.close();
            } catch (Exception e) {
               System.err.println(e);
            }
         }
         if (sc != null) {
            sc.close();
         }
      }
      try {
         conn.close();
      } catch (Exception e) {
         System.err.println(e);
      }

   }
}
