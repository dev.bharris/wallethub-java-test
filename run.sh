#!/bin/bash

cd src/main/java
javac com/ef/Parser.java 
jar cvf parser.jar com/ef/Parser.class com/ef/mysql-connector-java-8.0.11.jar 
java -cp com/ef/mysql-connector-java-8.0.11.jar -cp "parser.jar" com.ef.Parser --accesslog=/Users/dirac/src/personal/wallethub-java-test --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100


